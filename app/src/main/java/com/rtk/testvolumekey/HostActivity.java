package com.rtk.testvolumekey;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

/**
 * Created by charles-chang on 5/25/16.
 */
public abstract class HostActivity extends AppCompatActivity {

    private static String TAG = "TVOLKEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_VOLUME_UP) {
            Log.i(TAG,event.toString());
            return true;
        }

        return super.onKeyDown(keyCode,event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_VOLUME_UP) {
            Log.i(TAG,event.toString());
            return true;
        }

        return super.onKeyUp(keyCode,event);
    }

    public void onClick(View v) {

    }
}
